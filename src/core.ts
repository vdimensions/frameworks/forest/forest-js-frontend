import { Map } from "./common-types";

export const EMPTY_HIERARCHY: RegionMap = { "": [] };

export interface Command {
    name: string,
    path?: string,
    description?: string,
    displayName?: string,
    tooltip?: string
}

export type RegionMap = Map<string[]>;
export type CommandMap = Map<Command>;
export type ViewStateMap = Map<ViewState>;

export class ViewState {
    readonly name: string;
    readonly model: any;
    readonly regions: RegionMap;
    readonly commands: CommandMap;
    
    constructor({name = "", model = undefined, regions = {}, commands = {}}) {
        this.name = name;
        this.model = model;
        this.regions = regions;
        this.commands = commands;
    }
}